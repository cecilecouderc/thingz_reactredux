import React from "react";
import { connect } from "react-redux";
import { addTodo } from "../actions";

class AddTodo extends React.Component {
  constructor(props) {
    super(props);
    this.state = { dataInput: "" };
  }_

  updateInput = dataInput => {
    this.setState({ dataInput });
  };

  handleAddTodo = () => {
    this.props.dispatch(addTodo(this.state.dataInput));
    this.setState({ dataInput: "" });
  };

  render() {
    return (
      <div>
        <input
          type="text"
          onChange={e => this.updateInput(e.target.value)}
          placeholder={"Entrer une nouvelle tâche"}
          value={this.state.dataInput}
        />
        <button onClick={this.handleAddTodo}>
          ok
        </button>
      </div>
    );
  }
}
export default connect(
  null,
  null
  
)(AddTodo);