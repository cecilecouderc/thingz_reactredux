import React from 'react';
import {connect} from 'react-redux';

import { Checkbox, Button, Form, Input } from 'semantic-ui-react';

import { deleteTodo } from "../actions";

class Todo extends React.Component {

    constructor(props) {
        super(props);
            this.handleClick = this.handleClick.bind(this)
            this.handleCheckboxChange = this.handleCheckboxChange.bind(this)
    }

    handleCheckboxChange = (event) => {
        this.setState({ checked: event.target.checked});
        if (this.props.onCheck){
        this.props.onCheck(event.target.checked, this.props.id);  
        }
    }
    //ajout d'un évènement lié au bouton pour aller vers deletion
    handleClick = () => {
        this.props.deleteTodo(this.props.id)
        console.log("dispatch delete", this.props)

    }

    //     //prise en compte de ce qui est entré dans l'Input 
    // handleInputChange = (event) => {
    //     this.setState({dataInput: event.target.value});
    // }
    //je préviens le parent d'un évènement 
    // handleUpdateClick = (event) => {
    //     this.props.onUpdation(this.props.id, this.state.dataInput)
    // }
     //ajout fction callback
    // updateTodo = () => {this.props.onUpdation(this.props.dataInput) };
    
    render() {
        return (
            <div>
                <Checkbox
                    name="checked"
                    label={this.props.text}
                    checked={this.props.checked}
                    onChange={this.handleCheckboxChange}
                />
                <Button onClick={this.handleClick}>X</Button>  
                 
            </div>
        );
    }
}


export default Todo

