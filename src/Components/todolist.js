import React from 'react';
import {connect} from 'react-redux';

// import {todos} from '../reducers/todo'
import Todo from './todo';

import { deleteTodo } from  "../actions";


class TodoList extends React.Component {

    constructor(props) {
        super(props);
        // this.state = {
        //     list: [],
        //     doneList: [],
        //     dataInput: '',
        // };
        // this.deleteTodo = this.deleteTodo.bind(this)
        // this.checkTodo = this.checkTodo.bind(this)
        // this.addToDoneList = this.addToDoneList.bind(this)
        // this.updateTodo = this.updateTodo.bind(this)
    }
    //on passe dans le paramètre les données de l'enfant que l'on veut faire remonter 
  
    // deleteTodo(id) {
    //     let list = [...this.state.list];
    //     let index = -1;
    //     for(let i = 0; i < list.length; i++){
    //         if(list[i].id === id){
    //             index = i;
    //             break;
    //         }
    //     }
    //     if(index !== -1){           
    //         list.splice(index, 1);
    //         for(let i = 0; i < list.length; i++){
    //             list[i].id = i;
    //         }
    //     }
    //     this.setState({ list });
    // };

    updateTodo(id, title){
        // console.log(id)
        // console.log(title)
        let index = -1;
    
        for(let i = 0; i < this.state.list.length; i++){
            if(this.state.list[i].id === id){
                index = i;
                break; }
        }

        if(index !== -1){
            console.log("titre act", this.state.list[index].text);
            let list = [...this.state.list]
            list[index].text = title;
            this.setState({list});
        }
     
    }
    // addToDoneList(title) {
    //     // console.log("coucou")
    //     let doneList = [...this.state.doneList]
    //     doneList.push({text:title, id:this.state.doneList.length});
    //     this.setState({ doneList });
    // }
 
    // checkTodo(checked, id, title) {
    //     if (checked === true) {
    //         // alert("Checked")
    //         this.addToDoneList(title)
    //         this.deleteTodo(id)
    //         this.updateTodo(id, title)
    //     } else {
    //         alert("Unchecked")
    //     }
    // }

    deleteTodo = (id) => {
        this.props.dispatch({type : "DELETE_TODO", id : id })
    }

    checkingTodo = (checked, id) => {
        this.deleteTodo(id)
        this.props.dispatch({type : "ADD_TODO", id : id, text : this.props.todos[id].text, checked : checked})

    }


    renderToDo(){
        let tab=[];

        for (let i=0; i<this.props.todos.length; i++) {
            if (this.props.todos[i].checked === false){
            
            tab.push(<Todo 
            text = {this.props.todos[i].text}
            key={i}
            id={this.props.todos[i].id}
            checked={this.props.todos[i].checked}
            onCheck={this.checkingTodo}
            deleteTodo={this.deleteTodo} 
                    ></Todo>) 

            console.log(deleteTodo)   
            console.log(this.props.todos[i].text)
            }
        }
        
        return tab;
    }
    renderDone(){
        
        let tab=[];

        for (let i=0; i<this.props.todos.length; i++) {
            if (this.props.todos[i].checked === true){
            
            tab.push(<Todo 
            text = {this.props.todos[i].text}
            key={i}
            id={this.props.todos[i].id}
            checked={this.props.todos[i].checked}
            deleteTodo={this.deleteTodo} 
                    ></Todo>) 
                    console.log("props delete", this.props.deleteTodo)

            console.log(deleteTodo)   
            console.log(this.props.todos[i].text)
            }
        }
        return tab;
    }

    render() {

    //on transforme au préalable les données, en objet js, qui ne peuvent pas être affichées dans le render, donc on les rend utilisable dans un nouveau tableau
    
        return (
            <div>
                {/* {this.state.list.length} */}
                <p>{this.props.textTodo}</p>
                {/* <InputTodo onValidation={this.addTodo} /> */}
                <h2>A faire</h2>
                {this.renderToDo()}
                <h2>Fait</h2>
                {this.renderDone()}
        
            </div>
        );
    }
}



function mapStateToProps(state) {
    
    return { todos : state.todos}
    }
  
  export default connect(mapStateToProps)(TodoList)