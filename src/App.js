import React from 'react';

import './App.css';

import TodoList from './Components/todolist';
import Todo from './Components/todo';
import AddTodo from './Components/AddTodo';

function App() {

   return (
    <div>
      <h1>TodoList !</h1>
        <AddTodo/>
        <Todo/>
        <TodoList/>
    </div>

  );
}

export default App;
