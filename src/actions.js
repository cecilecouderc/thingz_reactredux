import {ADD_TODO, CHECK_TODO, DELETE_TODO} from './actionTypes'

let nextTodoId = 0

export const addTodo = text => ({
  type: ADD_TODO,
  id: nextTodoId++,
  text : text,
  checked : false
})

export const checkTodo = id => ({
  type: CHECK_TODO, 
  id: id,
  checked : false
})

export const deleteTodo = id => ({
  type: DELETE_TODO, 
  id : id
})

// export function addTodo(text) {
//   return {type: ADD_TODO, text }
// }

// export function checkTodo(id) {
//   return {type: CHECK_TODO, id }
// }