// exemple 

import AddTodo from '../Components/AddTodo';
import { ADD_TODO } from '../actions'


const todos = (state = [], action) => {
    switch (action.type) {

        case "ADD_TODO":
            return [
                ...state,
                {
                    id: action.id,
                    text: action.text,
                    checked: action.checked
                }
            ];
        case "CHECK_TODO":
            return [
                ...state,
                {
                    id: action.id,
                    checked: false
                }
            ]
        case "DELETE_TODO":

            console.log("DELETE_TODO")        
            console.log(action.id)  
            console.log(action)
            let list = [...state];

            let index = -1;


            for(let i = 0; i < list.length; i++){

                if(list[i].id === action.id){
                 
                    index = i;
                    break;
                }
            }
            if(index !== -1){           
                list.splice(index, 1);
                for(let i = 0; i < list.length; i++){
                    list[i].id = i;
                }
            }
            console.log(list)
            state = list;
            return state

        // case "UPDATE_TODO":
        //     [
        //         ...state,
        //         {
        //             id : action.id,
        //             text: action.text,
        //             checked: false
        //         }
        //     ]    
          // case "DELETE_TODO":
        //     [
        //         ...state,
        //         {
        //           id: ''
        //         }
        //       ]      

      default:
        return state
    }
  }
export default todos